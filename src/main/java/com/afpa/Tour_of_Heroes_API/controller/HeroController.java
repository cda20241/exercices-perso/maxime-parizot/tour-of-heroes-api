package com.afpa.Tour_of_Heroes_API.controller;

import com.afpa.Tour_of_Heroes_API.entity.Hero;
import com.afpa.Tour_of_Heroes_API.Repository.HeroRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Observable;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/hero")
public class HeroController {

    @Autowired
    private final HeroRepo heroRepo;

    public HeroController(HeroRepo heroRepo){
        this.heroRepo = heroRepo;
    }

    /**
     * Affiche tous les hero de la base de donnée
     * @return une liste de salaries
     */
    @GetMapping("/all")
    public List<Hero> getAllHeroes(){
        return heroRepo.findAll();
    }

    /**
     * Affiche un hero selon son id
     * @param id id de l'salarie
     * @return Une instance de salarie
     */
    @GetMapping("/{id}")
    public Hero getHero(@PathVariable Long id){
        Optional<Hero> hero = heroRepo.findById(id);
        if(hero.isPresent()){
            return hero.get();
        }
        else {
            return null;
        }
    }

    /**
     * Ajoute un hero a la base de donnée
     * @param hero le hero a ajouté
     * @return le hero ajouter
     */
    @PostMapping("/add")
    public Hero addHero(@RequestBody Hero hero){
        hero.setId(null);
        return heroRepo.save(hero);
    }

    /**
     * Met a jour un hero dans la base de donnée
     * @param hero le nouveau hero
     * @return le hero mise a jour
     */
    @PutMapping
    public Hero updateHero(@RequestBody Hero hero){
        return heroRepo.save(hero);
    }


    /**
     * Supprime un hero de la base de donnée
     * @param id l'id du salarie a supprimer
     * @return Un string
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteHero(@PathVariable Long id){
        Optional<Hero> hero = heroRepo.findById(id);
        if(hero.isPresent()){
            heroRepo.delete(hero.get());
            return new ResponseEntity<>("Hero Supprimé", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Hero non trouvé", HttpStatus.NOT_FOUND);
        }
    }
}

