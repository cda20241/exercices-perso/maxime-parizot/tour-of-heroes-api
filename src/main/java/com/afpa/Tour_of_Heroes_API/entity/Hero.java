package com.afpa.Tour_of_Heroes_API.entity;

import jakarta.persistence.*;

//public interface JoueurRepository extends JpaRepository<JoueurEntity,Long>
@Entity
@Table(name="hero")
public class Hero {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_hero", nullable = false, updatable = false)
    private Long id;

    @Column
    private String name;

    public Hero(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Hero(String name) {
        this.name = name;
    }

    public Hero() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
