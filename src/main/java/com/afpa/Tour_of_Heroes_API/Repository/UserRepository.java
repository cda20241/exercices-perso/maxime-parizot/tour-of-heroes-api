package com.afpa.Tour_of_Heroes_API.Repository;

import com.afpa.Tour_of_Heroes_API.Model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findUserByEmail(String email);

}
