package com.afpa.Tour_of_Heroes_API.Repository;

import com.afpa.Tour_of_Heroes_API.entity.Hero;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HeroRepo extends JpaRepository<Hero, Long> {
}
